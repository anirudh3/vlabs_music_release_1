(function(window) {
var sound1,sound2;
var path = '/static/soundfiles';
var playing = false;
var playing1 = false;
var current_note1=0;
var current_note2=0;
var correct = 0;
var incorrect = 0;
var tone_elem = document.createElement('audio');
var note_names = ["d1","d2","d3","d4","d5","d6","d7","d8","d9","d10","d11","d12","d13","d14","d15","d16","d17","d18",
"t1","t2","t3","t4","t5","t6","t7","t8","t9","t10","t11","t12","t13","t14","t15","t16","t17","t18","t19","t20","t21","t22","t23"];

function playsound() {
var notename1 =  note_names[Math.floor(Math.random() * note_names.length)];
var currentnote_path;
if(playing === true) {
 currentnote_path1 = path + '/'  + current_note1 + '.wav';
}
else {
    current_note1 = notename1;
    currentnote_path1 = path + '/' + current_note1 + '.wav';
    playing = true;
  }
tone_elem.setAttribute('src', currentnote_path1);
  tone_elem.play();

window.setTimeout(playsound2,2000);
}

function playsound2() {
var notename2 =  note_names[Math.floor(Math.random() * note_names.length)];
var currentnote_path2;
if(playing1 === true) {
 currentnote_path2 = path + '/'  + current_note2 + '.wav';
}
else {
    current_note2 = notename2;
    currentnote_path2 = path + '/' + current_note2 + '.wav';
    playing1 = true;
  }
tone_elem.setAttribute('src', currentnote_path2);
  tone_elem.play();

}

function submit() {

  if(playing == false || playing1 == false) {
    return;
  }
var selected = ($("input[type='radio'][name='swar1']:checked").val());

 var note1 = note_names.indexOf(current_note1);
 var note2 = note_names.indexOf(current_note2);

  if(note1>=0 && note1<=17 && note2>=0 && note2<=17 && selected == 'Both Dagga' || note1>=17 && note1<=40 && note2>=17 && note2<=40 && selected == 'Both Tabla' || note1>=0 && note1<=17 && note2>=17 && note2<=40 && selected == 'Tabla and Dagga' || note1>=17 && note1<=40 && note2>=0 && note2<=17 && selected == 'Tabla and Dagga') {
    $('#answer').html('Right');
    correct = correct + 1;
    playing = false;
    playing1 = false;
  }
  else {
    $('#answer').html('Wrong');
   
    incorrect = incorrect + 1;
    playing = false;
    playing1 = false;
  }

  var score = correct + incorrect;
  $('#score').html('Your score is ' + correct + ' out of ' + score);
  
}

window.playsound = playsound;
window.submit = submit;
})(window);

