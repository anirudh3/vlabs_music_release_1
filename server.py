# -*- coding: utf-8 -*-

from flask import Flask, render_template, make_response, request, session,\
    abort, jsonify, url_for, redirect

import json
import requests
from sqlalchemy.orm.exc import NoResultFound
import os
import logging
from logging.handlers import RotatingFileHandler

import config
from classes import User, db, Experiment
from datetime import datetime, timedelta


print "foo"


# function to create the Flask app
# useful in external scripts
def create_app():
    app = Flask(__name__)

    app.config['SQLALCHEMY_DATABASE_URI'] = config.DB_URI
    app.config['SECRET_KEY'] = config.secret_key

    db.init_app(app)
    app.db = db

    return app


# configure logging for this app
def configure_logging(app):

    formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s '
                                  '[in %(pathname)s:%(lineno)d]')

    # Also error can be sent out via email.
    # So we can also have a SMTPHandler?
    log_file = os.path.join(os.path.dirname(__file__), 'MusicLab.log')

    max_size = 1024 * 1024 * 20  # Max Size for a log file: 20MB
    log_handler = RotatingFileHandler(log_file, maxBytes=max_size, backupCount=10)

    try:
        if config.LOG_LEVEL:
            log_level = config.LOG_LEVEL
        else:
            log_level = 'DEBUG'
    except AttributeError:
        log_level = 'DEBUG'

    log_handler.setLevel(log_level)
    log_handler.setFormatter(formatter)

    app.logger.addHandler(log_handler)


# create our app
app = create_app()
app._key = config.SECRET_KEY
configure_logging(app)

# map to maintain experiment nos to their filenames
exp_num_file_map = {
    '1': '1_pitchdisc',
    '2': '2_pitchdir',
    '3': '3_sargam',
    '4': '4_thaat',
    '5': '5_pitchid',
    '6': '6_stringofsvar',
    '7': '7_sketch',
    '8': '8_ragavis',
    '9': '9_tabla',
    '10': '10_bols',
    '11':'11_quiz',
    '12':'12_taalquiz',
    '13':'13_instrumentquiz',
    '14':'14_layquiz'
}


@app.before_first_request
# populate experiments in the db from the above exp map
def populate_experiments():
    for exp_no, exp_name in exp_num_file_map.iteritems():
        if not Experiment.query.get(exp_no):
            filename = 'expr' + exp_name + '.html'
            new_exp = Experiment(id=exp_no, name=exp_name, filename=filename)
            new_exp.persist()




# the root URL
@app.route('/')
def index():
    return render_template('inheritance.html')


@app.route('/pages')
def start_page():
    return render_template('/pages/startpage.html')


@app.route('/pages/contact')
def contact():
    return render_template('pages/contact/contact.html')


@app.route('/experiments')
def render_explist():
    return render_template('pages/experiments.html')


@app.route('/experiments/<num>')
def render_experiment(num):

    if not User.getCurrentUser():
        return redirect(url_for('render_login_view', next=request.url))

    try:
        experiment = exp_num_file_map[num]
    except KeyError:
        abort(404)

    return render_template('experiments/expr' + experiment + '.html')

@app.route('/ragadb')
def render_ragadb():
    return render_template('ragadb.html')

@app.route('/login', methods=['GET'])
def render_login_view():
    if User.getCurrentUser():
        next_route = request.args.get('next')
        if next_route:
            return redirect(next_route)
        else:
            return redirect(url_for('index'))

    return render_template('login.html')


@app.route('/scores', methods=['POST'])
def save_score():
    current_user = User.getCurrentUser()
    if not current_user:
        abort(400, 'User has to be logged in to post scores!')

    if not request.form.get('score') or not request.form.get('experiment'):
        abort(400, 'Score and Experiment number are required!')

    score = request.form.get('score')
    experiment = request.form.get('experiment')
    level = request.form.get('level')

    try:
        current_user.save_score(score, experiment, level)
    except NoResultFound:
        abort(400, 'Wrong experiment number!')

    return make_response('200 OK', 200)


@app.route('/retention', methods=['POST'])
def save_retention():
    current_user = User.getCurrentUser()
    if not current_user:
        abort(400, 'User has to be logged in to save retention times!')

    if not request.form.get('start_time') or not request.form.get('end_time'):
        abort(400, 'start_time and end_time are required!')

    start_time = request.form.get('start_time')
    end_time = request.form.get('end_time')
    exp_num = request.form.get('experiment')

    try:
        current_user.save_retention(exp_num, start_time, end_time)
    except NoResultFound:
        abort(400, 'Wrong experiment number!')

    return make_response('200 OK', 200)


@app.route('/auth/login', methods=['POST'])
def user_login():
    response = make_response()

    if 'assertion' not in request.form:
        response.status_code = 400
        return response

    data = {'assertion': request.form['assertion'], 'audience':
            config.APP_URL}

    resp = requests.post(config.MOZ_PERSONA_VERIFIER, data=data, verify=True,
                         proxies=config.PROXIES)

    app.logger.debug('Response code from MOZ_PERSONA_VERIFIER %s' %
                     resp.status_code)
    app.logger.debug('Response body: %s' % resp.json())

    if resp.ok:
        verified_data = json.loads(resp.content)
        if verified_data['status'] == 'okay':
            user_email = verified_data['email']
            # check if this user exists in our system
            current_user = User.query.filter_by(email=user_email).first()
            # user doesn't exist; create her
            if current_user is None:
                app.logger.debug('user with email %s doesn\'t exist' %
                                 user_email)
                app.logger.debug('creating new user: %s' % user_email)

                # get the email_id and use it as a default username
                temp_username = user_email.split('@')[0]
                new_user = User(temp_username, user_email)
                new_user.persist()
                current_user = new_user

            app.logger.debug('logging in user with email %s' % user_email)
            session['email'] = current_user.email

            response.status_code = 200
            response.data = {'email': user_email}
            return response

    response.data = resp.text
    response.status_code = 500
    return response


@app.route('/auth/logout', methods=['POST'])
def user_logout():
    response = make_response()

    if 'email' in session:
        app.logger.debug('logging out user %s' % session['email'])
        session.pop('email')

    response.status_code = 200
    return response


#---------- wavesurfer start

@app.route('/annotation')
def render_annotation():
    return render_template('annotation.html')

#@app.route("/")
def index_wavesurfer():

    print 'In index printing session \n'

    print session
    print '\ndone\n'

    if 'auth_tok' in session:
        auth_tok = session['auth_tok']

        # check if it has expired
        oauth_token_expires_in_endpoint = config.swtstoreURL +\
            '/oauth/token-expires-in'
        resp = requests.get(oauth_token_expires_in_endpoint)
        expires_in = json.loads(resp.text)['expires_in']

        check = datetime.utcnow() - auth_tok['issued']
        if check > timedelta(seconds=expires_in):
            print 'access token expired'
            auth_tok = {'access_token': '', 'refresh_token': ''}
        else:
            print 'access token did not expire'

    else:
        auth_tok = {'access_token': '', 'refresh_token': ''}

    return render_template('index.html',
                           access_token=auth_tok['access_token'],
                           refresh_token=auth_tok['refresh_token'],
                           config=config)


@app.route("/downloadMusic")
def downloadMusic():

    url = request.args.get("url")
    print "in downloadMusic with content"
    print url
    url = url.strip()
    music_file_parse_url = url.split("/")
    music_file_name = music_file_parse_url[-1]
    print music_file_name
    music_file_path = 'static/music_files/' + music_file_name
    print music_file_path
    try:
        response = requests.get(url)
        data = response.content
    except Exception, e:
        print 'Sorry, we could not download the file! Please try again.'
        print e
        abort(500)

    try:
        open(music_file_path, 'wb').write(data)
    except:
        print 'error writing file'
        abort(500)

    return make_response(music_file_path, 200)


@app.route("/authenticate")
def authenticateWithOAuth():

    auth_tok = None
    code = request.args.get('code')

    print code

    # prepare the payload
    payload = {
        'scopes': 'email sweet',
        'client_secret': 'config.app_secret',
        'code': code,
        'redirect_uri': config.redirect_uri,
        'grant_type': 'authorization_code',
        'client_id': config.app_id
    }

    # token exchange endpoint
    oauth_token_x_endpoint = config.swtstoreURL + '/oauth/token'
    resp = requests.post(oauth_token_x_endpoint, data=payload)
    auth_tok = json.loads(resp.text)
    print 'recvd auth token from swtstore'
    print auth_tok

    if 'error' in auth_tok:
        print auth_tok['error']
        return make_response(auth_tok['error'], 200)

    # set sessions etc
    session['auth_tok'] = auth_tok
    session['auth_tok']['issued'] = datetime.utcnow()

    return redirect(url_for('index'))


@app.route("/signOut")
def signOut():
    user = request.args.get("user")

    print "in sign out the user\t"
    print user

    print session
    if 'auth_tok' in session:
        del(session['auth_tok'])
    print '\nsession deleted'
    print session
    print '\nnew session above'

    return redirect(url_for('index'))

#---- wavesurfer end

# Custom error handlers

@app.errorhandler(404)
def render_404(err):
    return render_template('errors/404.html', error=err.description)


@app.errorhandler(400)
def bad_request(err):
    return make_response(jsonify({'error': err.description}), 400)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0',  port=5002)
